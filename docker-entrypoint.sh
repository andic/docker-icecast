#!/bin/sh

echo "starting docker-entrypoint.sh"
echo "$(date)"
VERSION=$(icecast -v)
echo "${NAME} - ${VERSION}"


## create config
#if [ ${ICECAST_CREATE_CONFIG} == "true" ] || [ ${ICECAST_CREATE_CONFIG} == "true" ] || [ ${ICECAST_CREATE_CONFIG} -eq 1 ]; then
if [ ! -e /etc/icecast/icecast.xml ]; then
  envsubst < /usr/local/templates/icecast-template.xml > /etc/icecast/icecast.xml
fi

## start icecast
icecast -c /etc/icecast/icecast.xml
#su -c 'icecast -c /etc/icecast/icecast.xml' icecast

echo "stopping docker-entrypoint.sh"
echo "$(date)"
