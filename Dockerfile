ARG ARG_NAME=icecast
ARG ARG_VERSION=2.4.4


## Runtime Container
FROM alpine:3.14

LABEL org.opencontainers.image.authors="Andreas Cislik <git@a11k.net>"
LABEL org.opencontainers.image.source="https://gitlab.com/andic/docker-icecast"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.title="Icecast-Server"
LABEL org.opencontainers.image.description="Icecast streaming media server"

ARG ARG_NAME
ARG ARG_VERSION
ENV NAME=${ARG_NAME}
ENV VERSION=${ARG_VERSION}

ENV ICE_CREATE_CONFIG=false
ENV ICE_LOCATION=Earth
ENV ICE_ADMIN=icemaster@localhost
ENV ICE_ADMIN=master[at]a11k[dot]net
ENV ICE_CLIENTS=100
ENV ICE_SOURCES=2  
ENV ICE_SOURCE_PASSWORD=hackme
ENV ICE_RELAY_PASSWORD=hackme
ENV ICE_ADMIN_USER=admin
ENV ICE_ADMIN_PASSWORD=hackme
ENV ICE_HOSTNAME=localhost
ENV ICE_PORT=8000
ENV ICE_SSL=0
#ENV ICE_PORT=8443
#ENV ICE_SSL=1
ENV ICE_MASTER_SERVER=127.0.0.1
ENV ICE_MASTER_SERVER_PORT=8001
ENV ICE_UPDATE_INTERVAL=120
ENV ICE_MASTER_PASSWORD=hackme
ENV ICE_RELAYS_ON_DEMAND=0
#ENV ICE_CUSTOM=
ENV ICE_FILESERVE=1
ENV ICE_LOGLEVEL=3

ENTRYPOINT ["/docker-entrypoint.sh"]
VOLUME /etc/icecast
EXPOSE 8000

COPY docker-entrypoint.sh /docker-entrypoint.sh
COPY icecast-template.xml /usr/local/templates/icecast-template.xml

RUN set -ex \
  && echo "Building ${NAME} - ${VERSION}" \
  && apk update \
  && apk upgrade \
  && apk add \
    gettext \
    icecast \
    tzdata \
  && rm -rf /var/cache/apk/* \
  && chmod u+x /docker-entrypoint.sh \
  && addgroup icecast tty \
  && ln -fs /dev/stdout /var/log/icecast/access.log \
  && ln -fs /dev/stderr /var/log/icecast/error.log \
  && echo "Done building ${NAME} - ${VERSION}"

#USER icecast
